export default {
  home: 'Home',
  projects: 'Projects',
  experience: 'Experience',
  contacts: 'Contacts'
}
