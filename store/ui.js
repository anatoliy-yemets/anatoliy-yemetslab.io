export const state = () => ({
  drawer: false
})

export const mutations = {
  toggleDrawer (state) {
    state.drawer = !state.drawer
  },
  setDrawer (state, drawerState) {
    state.drawer = drawerState
  }
}
