import colors from 'vuetify/es5/util/colors'

const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  generate: {
    subFolders: false,
    dir: 'public'
  },

  router: {
    base
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Anatoliy Yemets',
    title: 'Portfolio',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'My portfolio. Currently looking for a position where I can grow as Front-end Developer. Keen on participating in interesting projects, working as a part of a big and creative team.' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/media/images/favicon/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/media/images/favicon/favicon-16x16.png' },
      { rel: 'icon', type: 'image/png', sizes: '48x48', href: '/media/images/favicon/favicon-48x48.png' },
      { rel: 'icon', type: 'image/png', sizes: '192x192', href: '/media/images/favicon/favicon-192x192.png' },
      { rel: 'icon', type: 'apple-touch-icon', sizes: '167x167', href: '/media/images/favicon/favicon-167x167.png' },
      { rel: 'icon', type: 'apple-touch-icon', sizes: '180x180', href: '/media/images/favicon/favicon-180x180.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Noto+Serif:wght@700&display=swap' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    '@nuxtjs/fontawesome',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/i18n'
  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    },
    icons: {
      iconfont: 'faSvg'
    }
  },

  i18n: {
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      redirectOn: 'root'
    },
    defaultLocale: 'en-US',
    vueI18nLoader: true,
    lazy: true,
    langDir: 'lang/',
    locales: [
      {
        code: 'en-US',
        name: 'English',
        file: 'en-US.js'
      },
      {
        code: 'uk-UA',
        name: 'Ukrainian',
        file: 'uk-UA.js'
      }
    ],
    vueI18n: {
      fallbackLocale: 'en-US',
      messages: {
        'en-US': {},
        'uk-UA': {}
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  vue: {
    config: {
      productionTip: false,
      devtools: true
    }
  }
}
